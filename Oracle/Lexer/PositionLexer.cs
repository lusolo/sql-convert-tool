﻿using CommonLib.Base;
using System.Text.RegularExpressions;

namespace Oracle.Lexer;

public class PositionLexer : BaseLexer
{
    public override bool Check()
        => Regex.IsMatch(Txt, "<<.*?>>");

    protected override BaseCodeEntity Run()
    {
        throw new NotImplementedException();
    }
}

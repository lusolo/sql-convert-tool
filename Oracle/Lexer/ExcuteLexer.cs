﻿using CommonLib.Base;

namespace Oracle.Lexer;

public class ExcuteLexer : BaseLexer
{
    public override bool Check()
        => Txt.StartsWith("EXECUTE", true, null);

    protected override BaseCodeEntity Run()
    {
        throw new NotImplementedException();
    }
}

﻿using CommonLib.Attr;
using CommonLib.Base;
using CommonLib.CodeEntity;
using CommonLib.Util;
using System.Text.RegularExpressions;

namespace Oracle.Lexer;

[Order(3)]
public class CursorLexer : BaseLexer
{
    public override bool Check()
        => Txt.StartsWith("OPEN ") || Txt.StartsWith("CLOSE ") || Txt.StartsWith("CURSOR ");

    protected override BaseCodeEntity Run()
    {
        var entity = new CursorCodeEntity();
        var txt = Txt;
        if (txt.StartsWith("CURSOR"))
        {
            entity.Type = "CURSOR";
            entity.Name = Regex.Match(txt, "CURSOR(.*?)\\b[A|I]S").Result("$1").Trim();
            var end = Regex.Match(txt, "[A|I]S\\b(.*)").Result("$1").Trim();
            entity.Body = SqlFactory.Create(end).Execute();
        }
        return entity;
    }
}

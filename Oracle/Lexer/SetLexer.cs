﻿using CommonLib.Attr;
using CommonLib.Base;
using CommonLib.CodeEntity;
using CommonLib.Util;

namespace Oracle.Lexer;

[Order(3)]
public class SetLexer : BaseLexer
{
    public override bool Check()
    {
        if (Real.Contains(":="))
        {
            var arr = Real.TrimSplit(":=");
            return !arr[0].Contains(' ');
        }
        return false;
    }

    protected override BaseCodeEntity Run()
    {
        var entity = new SetCodeEntity();
        entity.EndComment = Comment;
        var code = Txt.Replace(";", "");
        var arr = code.TrimSplit(":=");
        entity.Left = SqlFactory.Create(arr[0]).Execute();
        entity.Right = SqlFactory.Create(arr[1]).Execute();
        return entity;
    }
}

﻿using CommonLib.Attr;
using CommonLib.Base;
using CommonLib.CodeEntity;
using CommonLib.Util;
using Oracle.Read;
using System.Text.RegularExpressions;

namespace Oracle.Lexer;

[Order(1)]
public sealed class BeginBodyLexer : BaseLexer
{
    public override bool Check()
        => Txt.StartsWith("BEGIN");

    protected override BaseCodeEntity Run()
    {
        var entity = new BeginCodeEntity();
        var txt = Regex.Match(Txt,"BEGIN(.*)END").Result("$1").Trim();
        var arr = txt.TrimSplit('÷').ToList();
        var reader = new SqlLineRead();
        reader.Read(arr);
        while (reader.HasNext)
        {
            entity.Body.Add(SqlFactory.Create(reader.Next).Execute());
        }
        return entity;
    }
}

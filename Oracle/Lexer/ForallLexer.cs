﻿using CommonLib.Attr;
using CommonLib.Base;

namespace Oracle.Lexer;

[Order(5)]
public class ForallLexer : BaseLexer
{
    public override bool Check()
        => Txt.StartsWith("FORALL ");

    protected override BaseCodeEntity Run()
    {
        throw new NotImplementedException();
    }
}

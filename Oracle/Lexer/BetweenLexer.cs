﻿using CommonLib.Attr;
using CommonLib.Base;
using CommonLib.CodeEntity;
using CommonLib.Util;

namespace Oracle.Lexer;

[Order(3)]
[Rank(CommonLib.Enum.LexerKind.All)]
public class BetweenLexer : BaseLexer
{
    public override bool Check()
        => Txt.Contains(" BETWEEN ");

    protected override BaseCodeEntity Run()
    {
        var entity = new BetweenCodeEntity();
        var txt = Txt;
        var head = txt[..txt.IndexOf(" BETWEEN")].Trim();
        entity.Head = SqlFactory.Create(head).Execute();
        txt = txt[txt.IndexOf(" BETWEEN")..].Trim();
        var arr = txt.TrimSplit("AND");
        int idx = 0;
        var pre = arr[idx];
        while (!pre.IsFull())
        {
            pre = $"{pre} AND {arr[++idx]}";
        }
        entity.PreCode = SqlFactory.Create(pre).Execute();
        var suf = string.Join(" AND ", arr.Skip(idx));
        entity.SufCode = SqlFactory.Create(suf).Execute();
        return entity;
    }
}

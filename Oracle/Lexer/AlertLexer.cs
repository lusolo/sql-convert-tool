﻿using CommonLib.Attr;
using CommonLib.Base;
using CommonLib.CodeEntity;

namespace Oracle.Lexer;

[Order(2)]
public class AlertLexer : BaseLexer
{
    public override bool Check()
        => Txt.StartsWith("ALTER ");

    protected override BaseCodeEntity Run()
    {
        var entity = new AlertCodeEntity();
        entity.IsDelete = true;
        return entity;
    }
}

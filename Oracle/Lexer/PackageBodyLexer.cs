﻿using CommonLib.Attr;
using CommonLib.Base;

namespace Oracle.Lexer;

[Order(0)]
public class PackageBodyLexer : BaseLexer
{
    public override bool Check()
        => Txt.StartsWith("CREATE OR REPLACE PACKAGE BODY");

    protected override BaseCodeEntity Run()
    {
        throw new NotImplementedException();
    }
}

﻿using CommonLib.Attr;
using CommonLib.Base;

namespace Oracle.Lexer;

[Order(0)]
public class PackageLexer : BaseLexer
{
    public override bool Check()
        => Txt.StartsWith("CREATE OR REPLACE PACKAGE");

    protected override BaseCodeEntity Run()
    {
        throw new NotImplementedException();
    }
}

﻿using CommonLib.Attr;
using CommonLib.Base;
using CommonLib.CodeEntity;
using CommonLib.Info;
using System.Text.RegularExpressions;

namespace Oracle.Lexer;

[Order(5)]
public class ConstLexer : BaseLexer
{
    public override bool Check()
        => Txt.StartsWith("STRLIST_") || Regex.IsMatch(Txt, "^(-)?[0-9.]+$");

    protected override BaseCodeEntity Run()
    {
        var entity = new ConstCodeEntity();
        if (Txt.StartsWith("STRLIST_"))
        {
            entity.Kind = "string";
            entity.Vaule = SqlStringList.GetItem(Txt);
        }
        else
        {
            entity.Kind = "num";
            entity.Vaule = Txt;
        }
        return entity;
    }
}

﻿using CommonLib.Attr;
using CommonLib.Base;

namespace Oracle.Lexer;

[Order(5)]
[Rank(CommonLib.Enum.LexerKind.Line)]
public class RaiseLexer : BaseLexer
{
    public override bool Check()
        => Txt.StartsWith("RAISE ") || Txt.StartsWith("RAISE;");

    protected override BaseCodeEntity Run()
    {
        throw new NotImplementedException();
    }
}

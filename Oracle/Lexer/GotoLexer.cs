﻿using CommonLib.Base;
using CommonLib.CodeEntity;

namespace Oracle.Lexer;

public class GotoLexer : BaseLexer
{
    public override bool Check()
        => Txt.StartsWith("GOTO ");

    protected override BaseCodeEntity Run()
    {
        var entity = new GotoCodeEntity
        {
            EndComment = Comment,
            Position = Txt[4..].Trim()
        };
        return entity;
    }
}

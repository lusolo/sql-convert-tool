﻿using CommonLib.Attr;
using CommonLib.Base;
using CommonLib.CodeEntity;
using CommonLib.Util;
using System.Text.RegularExpressions;

namespace Oracle.Lexer;

[Order(6)]
public class VerbLexer : BaseLexer
{
    public override bool Check()
    {
        if( OracleConst.KeyWords.Any(s => s.Equals(Real))) return true;
        //if(ConstUtil.IsSql && !Regex.IsMatch(Txt,"[^A-Z0-9_.]")) return true;
        if(Txt.StartsWith(":NEW.") || Txt.StartsWith(":OLD.")) return true;
        if (!Regex.IsMatch(Txt, "[^A-Z0-9_.]")) return true;
        return false;
    }

    protected override BaseCodeEntity Run()
    {
        var type = string.Empty;
        if (Txt.EqualsIgnoreCase("sysdate")) type = "SysDate";       
        return new VerbCodeEntity(Txt, type);
    }
}

﻿using CommonLib.Attr;
using CommonLib.Base;
using CommonLib.CodeEntity;
using CommonLib.Util;

namespace Oracle.Lexer;

[Order(4)]
public class WhenLexer : BaseLexer
{
    public override bool Check()
        => Txt.StartsWith("WHEN");

    protected override BaseCodeEntity Run()
    {
        var entity = new WhenCodeEntity();
        var txt = Txt[4..].Trim();
        var head = txt[..txt.IndexOf(" THEN")].Trim();
        entity.Head = SqlFactory.Create(head).Execute();
        return entity;
    }
}

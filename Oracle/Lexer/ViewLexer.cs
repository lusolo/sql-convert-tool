﻿using CommonLib.Attr;
using CommonLib.Base;
using CommonLib.CodeEntity;
using CommonLib.Util;
using System.Text.RegularExpressions;

namespace Oracle.Lexer;

[Order(0)]
public class ViewLexer : BaseLexer
{
    public override bool Check()
        => Regex.IsMatch(Txt, "CREATE.*?VIEW");

    protected override BaseCodeEntity Run()
    {
        var view = Txt.Replace("\"", "").RealSql();
        var name = view[..view.IndexOf("(")].Trim();
        name = name[(name.IndexOf(".") + 1)..].Trim();
        var columns = Regex.Match(view, "\\((.*?)\\)").Result("$1").Trim();
        var entity = new ViewCodeEntity(name, columns);
        var sel = view[view.IndexOf(" SELECT")..].Trim();
        entity.SqlBody = SqlFactory.Create(sel).Execute();
        return entity;
    }
}

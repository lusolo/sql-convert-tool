﻿namespace Oracle;

public static class OracleConst
{

    public static readonly List<string> KeyWords = new()
    {
        "SQLCODE","SQLERRM","SQL%ROWCOUNT","SYSDATE","COMMIT","ROLLBACK","NULL","NO_DATA_FOUND",
        "EXCEPTION","INSERTING","UPDATING","DELETING"
    };


    public static readonly List<string> SysFunction = new()
    {
        "TO_DATE","TO_CHAR","DECODE","NVL","NVL2","SUBSTR","LPAD","RPAD","TRIM",
        "ASCII","CHR","CONCAT","INSTR","INITCAP","LENGTH","UPPER","LOWER","LTRIM",
        "RTRIM","REPLACE","ABS","ACOS","COS","CEIL","FLOOR","LOG","MOD","POWER",
        "ROUND","SQRT","TRUNC","ADD_MONTHS","LAST_DAY","EXTRACT","TO_NUMBER"
    };

    public static readonly List<string> Marks = new()
    {
        "+","-","*","/","||","<>",">","<",">=","<=","!=", " AND "," OR "," IS ", "="
    };

}

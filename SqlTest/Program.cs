﻿// See https://aka.ms/new-console-template for more information
using CommonLib.Base;
using CommonLib.Util;
using System.Reflection;

Console.WriteLine("Hello, World!");

ConstUtil.InPath = @"C:\Users\Administrator\Desktop\out\store";
ConstUtil.OutPath = @"C:\Users\Administrator\Desktop\post\store";
ConstUtil.Before = "Oracle";
ConstUtil.After = "PostgreSQL";
ConstUtil.DataBase = "IP2";
ConstUtil.ConnectionString = "User Id=IP2;Password=123456;Data Source=(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.7.194)(PORT=1521)))(CONNECT_DATA=(SERVICE_NAME=orcl)))";
Assembly assembly = Assembly.Load("Oracle");
var obj = assembly.CreateInstance($"Oracle.Read.OracleFilesRead");
if (obj != null && obj is SqlFilesRead reader)
{
    reader.Read(null, assembly);
}
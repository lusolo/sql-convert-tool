﻿using CommonLib.Base;
using CommonLib.CodeEntity;

namespace PostgreSQL.Convert;

public class BracketConvert(BracketCodeEntity bracket) : BaseConvert
{
    private readonly BracketCodeEntity Bracket = bracket;

    protected override List<string> Run()
    {
        var txt = Bracket.Code.SqlLine();
        return SingleLine($"({txt})");
    }
}

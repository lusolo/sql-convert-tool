﻿using CommonLib.Base;
using CommonLib.CodeEntity;

namespace PostgreSQL.Convert;

public class BetweenConvert(BetweenCodeEntity between) : BaseConvert
{
    private readonly BetweenCodeEntity Between = between;

    protected override List<string> Run()
    {
        var head = Between.Head!.SqlLine();
        var pre = Between.PreCode!.SqlLine();
        var suf = Between.SufCode!.SqlLine();
        return SingleLine($"{head} BETWEEN {pre} AND {suf}");
    }
}

﻿using CommonLib.Base;
using CommonLib.CodeEntity;

namespace PostgreSQL.Convert;

public class IfConvert(IfCodeEntity ifCode) : BaseConvert
{
    private readonly IfCodeEntity ifCode = ifCode;

    protected override List<string> Run()
    {
        var list = new List<string>();
        var head = ifCode.Head.SqlLine();
        list.Add($"IF {head} THEN" );
        ifCode.Body.ForEach(x => list.AddRange(x.SqlList()));
        if (ifCode.ElseIf != null)
        {
            foreach(var item in ifCode.ElseIf)
            {
                var sub = item.SqlList();
                sub[0] = $"ELS{sub[0]}";
                list.AddRange(sub);
            }
        }
        if (ifCode.Else != null)
        {
            list.Add("ELSE");
            ifCode.Else.ForEach(x => list.AddRange(x.SqlList()));
        }      
        list.Add("END IF");
        return list;
    }
}

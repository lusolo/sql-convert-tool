﻿using CommonLib.Base;
using CommonLib.CodeEntity;

namespace PostgreSQL.Convert;

public class ViewConvert(ViewCodeEntity view) : BaseConvert
{
    private readonly ViewCodeEntity View = view;

    protected override List<string> Run()
    {
        var list = new List<string>
        {
            $"CREATE VIEW {View.Name}(",
            View.Columns,
            ") AS"
        };
        list.AddRange(View.SqlBody!.SqlList());
        return list;
    }
}

﻿using CommonLib.Base;
using CommonLib.CodeEntity;

namespace PostgreSQL.Convert;

public class AndConvert(AndCodeEntity and) : BaseConvert
{

    private readonly AndCodeEntity And = and;

    protected override List<string> Run()
    {
        var list = new List<string>();
        for (int i = 0; i < And.Condition.Count; i++)
        {
            var con = And.Condition[i].SqlLine();
            var com = And.Comments[i];
            if (com.Contains('÷')) list.Add($"{con}");
            else if (com.Contains(" IN"))
            {
                if (string.IsNullOrEmpty(con)) list.Add(com);
                else list.Add($"AND {com}({con})");
            }
            else if (com.Contains("EXISTS")) list.Add($"AND {com}({con})");
            else list.Add($"{con} {com}");
        }
        var txt = string.Join(" ", list);
        if (txt.EndsWith(" AND")) txt = txt[..^3].Trim();
        if (And.HasBracket) return SingleLine($"{And.Mark} ({txt})");
        return SingleLine($"{And.Mark} {txt}");
    }
}

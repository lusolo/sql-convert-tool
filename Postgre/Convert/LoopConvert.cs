﻿using CommonLib.Base;
using CommonLib.CodeEntity;

namespace PostgreSQL.Convert;

public class LoopConvert(LoopCodeEntity loop) : BaseConvert
{
    private readonly LoopCodeEntity Loop = loop;

    protected override List<string> Run()
    {
        var list = new List<string>();
        if (Loop.Idx != null && Loop.Condition != null)
        {
            list.Add($"FOR {Loop.Idx} IN {Loop.Condition.SqlLine()} LOOP");
        }
        else if (Loop.Condition != null)
        {
            list.Add($"WHILE {Loop.Condition.SqlLine()} LOOP");
        }
        else list.Add("LOOP");
        Loop.Body.ForEach(b => list.AddRange(b.SqlList()));
        list.Add("END LOOP");
        return list;
    }
}

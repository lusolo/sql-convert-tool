﻿using CommonLib.Base;
using CommonLib.CodeEntity;

namespace PostgreSQL.Convert;

public class CursorConvert(CursorCodeEntity cursor) : BaseConvert
{

    private readonly CursorCodeEntity Cursor = cursor;

    protected override List<string> Run()
    {
        var list = new List<string>();
        if (Cursor.Type.Equals("Cursor"))
        {
            list.Add($"{Cursor.Name} CURSOR {Cursor.Body!.SqlLine()}");
        }
        return list;
    }
}

﻿using CommonLib.Base;
using CommonLib.CodeEntity;

namespace PostgreSQL.Convert;

public class ConditionConvert(ConditionCodeEntity condition) : BaseConvert
{
    private readonly ConditionCodeEntity Condition = condition;

    protected override List<string> Run()
    {
        var txt = Condition.Cons[0].SqlLine();
        for (int i = 1; i < Condition.Cons.Count; i++)
        {
            var item = Condition.Cons[i].SqlLine();
            if (i< Condition.Cons.Count)
            {
                txt = $"{txt} {Condition.Marks[i-1]} {item}";
            }
            else txt = $"{txt} {item}";
        }
        txt = txt.Replace("||","+");
        return SingleLine(txt);
    }
}

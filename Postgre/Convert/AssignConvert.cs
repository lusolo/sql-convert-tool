﻿using CommonLib.Base;
using CommonLib.CodeEntity;
using CommonLib.Util;

namespace PostgreSQL.Convert;

public class AssignConvert(AssignCodeEntity assign) : BaseConvert
{
    private readonly AssignCodeEntity Assign = assign;

    protected override List<string> Run()
    {
        var type = Assign.Type;
        type = TypeConvertor.NewType(type!);
        return SingleLine($"{Assign.Name} {type}");
    }
}

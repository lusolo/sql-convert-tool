﻿using CommonLib.Base;
using CommonLib.CodeEntity;

namespace PostgreSQL.Convert;

public class VerbConvert(VerbCodeEntity verb) : BaseConvert
{
    private readonly VerbCodeEntity Verb = verb;

    protected override List<string> Run()
    {
        var val = Verb.Value;
        if (val.Equals("NONE")) val = string.Empty;
        else if (val.Equals("SYSDATE")) val = "NOW()";
        else if (val.Equals("INSERTING")) val = "TG_OP = 'INSERT'";
        else if (val.Equals("UPDATING")) val = "TG_OP = 'UPDATE'";
        else if (val.Equals("DELETING")) val = "TG_OP = 'DELETE'";
        else if (val.Equals("NO_DATA_FOUND")) val = "NOT FOUND";
        else if(val.StartsWith(":")) val =val[1..];
        return SingleLine(val);
    }
}

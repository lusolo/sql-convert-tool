﻿using CommonLib.Base;
using CommonLib.CodeEntity;

namespace PostgreSQL.Convert;

public class BeginConvert(BeginCodeEntity begin) : BaseConvert
{
    private readonly BeginCodeEntity Begin = begin;

    protected override List<string> Run()
    {
        var list = new List<string>
        {
            "BEGIN"
        };
        Begin.Body.ForEach(s =>
        {
            list.AddRange(s.SqlList());
        });
        list.Add("END;");
        return list;
    }
}

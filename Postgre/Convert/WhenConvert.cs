﻿using CommonLib.Base;
using CommonLib.CodeEntity;

namespace PostgreSQL.Convert;

public class WhenConvert(WhenCodeEntity when) : BaseConvert
{
    private readonly WhenCodeEntity When = when;

    protected override List<string> Run()
    {
        var list = new List<string>();
        var head = When.Head!.SqlLine();
        list.Add($"WHEN {head} THEN");
        return list;
    }
}

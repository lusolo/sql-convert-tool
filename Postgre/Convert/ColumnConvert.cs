﻿using CommonLib.Base;
using CommonLib.CodeEntity;
using CommonLib.Util;

namespace PostgreSQL.Convert;

public class ColumnConvert(ColumnCodeEntity column) : BaseConvert
{
    private readonly ColumnCodeEntity Column = column;

    protected override List<string> Run()
    {
        var col = $"{Column.Name} {TypeConvertor.NewType(Column.Kind)}";
        if (Column.IsNotNull) col = $"{col} NOT NULL";
        if (Column.DeflutValue!= null) col = $"{col} DEFAULT {Column.DeflutValue.SqlLine()}";
        return SingleLine(col);
    }
}

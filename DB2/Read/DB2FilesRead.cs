﻿using CommonLib.Base;
using CommonLib.CodeEntity;
using CommonLib.Util;

namespace DB2.Read;

public class DB2FilesRead : SqlFilesRead
{
    protected override RootCodeEntity ExecuteLexer(List<string> Lines)
    {
        var entity = new RootCodeEntity();
        var reader = new SqlLineRead();
        reader.Read(Lines);
        while (reader.HasNext)
        {
            entity.Body.Add(SqlFactory.Create(reader.Next).Execute());
        }
        return entity;
    }

    protected override List<string> FormatLines(List<string> Lines)
    {
        throw new NotImplementedException();
    }
}

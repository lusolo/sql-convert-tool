﻿using System.IO;
using System.Text;

namespace CommonLib.Util;

public class FileCode
{
    public static Encoding GetCode(string file)
    {
        using var fs = File.OpenRead(file);
        Ude.CharsetDetector cdet = new();
        cdet.Feed(fs);
        cdet.DataEnd();
        var code = cdet.Charset ?? "UTF-8";
        return Encoding.GetEncoding(code);
    }
}

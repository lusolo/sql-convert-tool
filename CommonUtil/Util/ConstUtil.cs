﻿namespace CommonLib.Util;

public static class ConstUtil
{
    public static string InPath = string.Empty;

    public static string OutPath = string.Empty;

    public static string Before = string.Empty;

    public static string After = string.Empty;

    public static string Encoding = string.Empty;

    public static string DataBase = string.Empty;

    public static string ConnectionString = string.Empty;

    public static bool IsSql = false;

    public static object? SwitchObj = null;

    public static void SetSql(bool sql, object obj)
    {
        if (sql && SwitchObj == null)
        {
            SwitchObj = obj;
            IsSql = true;
        }
        else if (obj.Equals(SwitchObj))
        {
            IsSql = false;
            SwitchObj = null;
        }
    }
}

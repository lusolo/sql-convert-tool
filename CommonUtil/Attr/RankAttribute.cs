﻿using CommonLib.Enum;
using System;

namespace CommonLib.Attr;

[AttributeUsage(AttributeTargets.Class)]
public class RankAttribute : Attribute
{
    public LexerKind Kind { get; set; }

    public RankAttribute(LexerKind kind)
    {
        Kind = kind;
    }
}

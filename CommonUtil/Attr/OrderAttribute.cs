﻿using System;

namespace CommonLib.Attr;

[AttributeUsage(AttributeTargets.All)]
public class OrderAttribute : Attribute
{
    public int Index { get; set; }

    public OrderAttribute(int index)
    {
        Index = index;
    }
}

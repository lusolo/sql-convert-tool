﻿namespace CommonLib.Enum;

public enum LexerKind
{
    Block,
    Line,
    All,
}

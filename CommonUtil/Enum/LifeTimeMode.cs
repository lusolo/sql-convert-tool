﻿namespace CommonLib.Enum;

public enum LifeTimeMode
{
    Singleton,
    Transient
}

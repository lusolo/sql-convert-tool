﻿using System;

namespace CommonLib.Base;

public class UnRecognizedLexer : BaseLexer
{
    public override bool Check()
        => false;

    protected override BaseCodeEntity Run()
    {
        throw new NotImplementedException();
    }
}

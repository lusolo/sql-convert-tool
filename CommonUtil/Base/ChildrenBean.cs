﻿using CommonLib.Attr;
using System.Reflection;

namespace CommonLib.Base;

public class ChildrenBean
{
    public int Order { get; set; }

    public BaseCodeEntity CurrentEntity { get; set; }


    public object? OrderEntity { get; set; }


    public ChildrenBean(int order, BaseCodeEntity entity)
    {
        Order = order;
        CurrentEntity = entity;
        if (entity == null) return;
        var parent = entity.ParentEntity;
        if (parent == null) return;
        foreach (var prop in parent.GetType().GetProperties())
        {
            var idx = prop.GetCustomAttribute<OrderAttribute>();
            if (idx != null && idx.Index == order)
            {
                OrderEntity = prop.GetValue(parent);
            }
        }
    }
}

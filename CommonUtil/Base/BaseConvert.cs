﻿using System;
using System.Collections.Generic;

namespace CommonLib.Base;

public abstract class BaseConvert
{

    public List<string> SqlList()
    {
        try
        {
            return Run();
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
            throw;
        }
    }

    public string SqlLine()
    {
        try
        {
            return string.Join("", Run());
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
            throw;
        }
    }

    protected abstract List<string> Run();

    public List<string> SingleLine(string sql) => new() { sql };
}

﻿using CommonLib.Attr;
using CommonLib.Base;
using CommonLib.CodeEntity;
using CommonLib.Util;
using System.Text.RegularExpressions;

namespace CommonLib.Lexer;

[Order(1)]
public class DeleteLexer : BaseLexer
{
    public override bool Check()
        => Txt.StartsWith("DELETE ") || Txt.StartsWith("DROP ");

    protected override BaseCodeEntity Run()
    {
        var entity = new DeleteCodeEntity();
        var head = Real.TrimEnd(';');

        if (head.Contains(" WHERE"))
        {
            var end = Regex.Match(head, "WHERE.*").Value;
            head = Regex.Match(head, "(.*?)WHERE").Result("$1").Trim();
            entity.Where = SqlFactory.Create(end).Execute();
        }
        entity.Head = head;
        return entity;
    }
}

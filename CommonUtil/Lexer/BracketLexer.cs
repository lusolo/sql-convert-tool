﻿using CommonLib.Attr;
using CommonLib.Base;
using CommonLib.CodeEntity;
using CommonLib.Util;

namespace CommonLib.Lexer;

[Order(5)]
public class BracketLexer : BaseLexer
{
    public override bool Check()
        => Txt.StartsWith('(') && Txt.EndsWith(")");

    protected override BaseCodeEntity Run()
    {
        var code = Txt[1..^1].Trim();
        var obj = SqlFactory.Create(code).Execute();
        return new BracketCodeEntity(obj);
    }
}

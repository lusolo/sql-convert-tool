﻿using CommonLib.Attr;
using CommonLib.Base;
using CommonLib.CodeEntity;
using System;

namespace CommonLib.Lexer;

[Order(1)]
public class CommentLexer : BaseLexer
{
    public override bool Check()
        => Txt.StartsWith("--") || Txt.StartsWith("/*");

    protected override BaseCodeEntity Run()
    {
        var entity =new CommentCodeEntity();
        entity.Comments.AddRange(Txt.Split('÷'));
        return entity;
    }
}

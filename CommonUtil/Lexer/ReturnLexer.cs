﻿using CommonLib.Attr;
using CommonLib.Base;
using CommonLib.CodeEntity;
using CommonLib.Util;

namespace CommonLib.Lexer;

[Order(5)]
public class ReturnLexer : BaseLexer
{
    public override bool Check()
        => Txt.StartsWith("RETURN ") || Txt.StartsWith("RETURN;");

    protected override BaseCodeEntity Run()
    {
        var entity = new ReturnCodeEntity();
        entity.EndComment = Comment;
        if (Txt.Length > 7)
        {
            var code = Txt[6..].TrimEnd(';').Trim();
            entity.Code = SqlFactory.Create(code).Execute();
        }
        return entity;
    }
}

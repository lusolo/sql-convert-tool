﻿using System.Collections.Generic;

namespace CommonLib.Info;

public class TabelInfo
{
    public string Name { get; set; }

    public Dictionary<string, string> Columns { get; set; }

    public TabelInfo(string name)
    {
        Name = name;
        Columns = new();
    }
}

﻿using CommonLib.Base;

namespace CommonLib.CodeEntity;

public class DeleteCodeEntity : BaseCodeEntity
{
    public string? Head {  get; set; }

    public BaseCodeEntity? Where { get; set; }
}

﻿using CommonLib.Attr;
using CommonLib.Base;
using System.Collections.Generic;

namespace CommonLib.CodeEntity;

public class InsertCodeEntity : BaseCodeEntity
{
    public string? Head { get; set; }

    [Order(0)]
    public List<BaseCodeEntity> Values { get; set; }

    public InsertCodeEntity() => Values = new();
}

﻿using CommonLib.Attr;
using CommonLib.Base;
using System.Collections.Generic;

namespace CommonLib.CodeEntity;

public class IfCodeEntity : BaseCodeEntity
{
    [Order(0)]
    public BaseCodeEntity Head { get; set; }

    [Order(1)]
    public List<BaseCodeEntity> Body { get; set; }

    [Order(2)]
    public List<BaseCodeEntity>? ElseIf { get; set; }

    [Order(3)]
    public List<BaseCodeEntity>? Else { get; set; }

    public IfCodeEntity(BaseCodeEntity head)
    {
        Head = head;
        Body = new();
    }
}

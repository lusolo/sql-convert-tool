﻿using CommonLib.Base;
using System.Collections.Generic;

namespace CommonLib.CodeEntity;

public class UpdateCodeEntity : BaseCodeEntity
{
    public string? Head { get; set; }

    public List<BaseCodeEntity> Vaules { get; set; }

    public List<string> Cols { get; set; }

    public BaseCodeEntity? Where { get; set; }

    public UpdateCodeEntity()
    {
        Vaules = [];
        Cols = [];
    }
}

﻿using CommonLib.Base;
using System.Collections.Generic;

namespace CommonLib.CodeEntity;

public class LoopCodeEntity : BaseCodeEntity
{
    public string? Idx { get; set; }

    public BaseCodeEntity? Condition { get; set; }

    public List<BaseCodeEntity> Body { get; set; }

    public LoopCodeEntity()
    {
        Body = [];
    }
}

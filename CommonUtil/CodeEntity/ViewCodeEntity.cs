﻿using CommonLib.Attr;
using CommonLib.Base;

namespace CommonLib.CodeEntity;

public class ViewCodeEntity : BaseCodeEntity
{
    public string Name { get; set; }

    public string Columns { get; set; }

    [Order(0)]
    public BaseCodeEntity? SqlBody { get; set; }

    public ViewCodeEntity(string name, string columns)
    {
        Columns = columns;
        Name = name;
    }

}

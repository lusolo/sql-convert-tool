﻿using CommonLib.Base;

namespace CommonLib.CodeEntity;

public class SetCodeEntity : BaseCodeEntity
{
    public BaseCodeEntity? Left { get; set; }

    public BaseCodeEntity? Right { get; set; }

}

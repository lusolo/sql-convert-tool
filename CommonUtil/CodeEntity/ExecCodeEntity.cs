﻿using CommonLib.Base;
using System.Collections.Generic;

namespace CommonLib.CodeEntity;

public class ExecCodeEntity : BaseCodeEntity
{

    public string Name { get; set; }

    public string Kind { get; set; }

    public List<BaseCodeEntity> Params { get; set; }

    public ExecCodeEntity(string name, string kind)
    {
        this.Name = name;
        this.Kind = kind;
        Params = new();
    }

}

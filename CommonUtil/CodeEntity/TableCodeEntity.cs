﻿using CommonLib.Attr;
using CommonLib.Base;
using System.Collections.Generic;

namespace CommonLib.CodeEntity;

public class TableCodeEntity : BaseCodeEntity
{
    public string Name { get; set; }

    public string? PrimayKey { get; set; }

    [Order(0)]
    public List<ColumnCodeEntity> Columns { get; set; }

    public TableCodeEntity(string name)
    {
        this.Name = name;
        Columns = new();
    }
}

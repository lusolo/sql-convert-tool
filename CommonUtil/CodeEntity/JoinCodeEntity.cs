﻿using CommonLib.Base;
using System.Collections.Generic;

namespace CommonLib.CodeEntity;

public class JoinCodeEntity : BaseCodeEntity
{
    public  List<BaseCodeEntity> TblCode { get; set; }

    public List<string> Jstr { get; set; }

    public List<BaseCodeEntity> Cons { get; set; }

    public JoinCodeEntity()
    {
        Jstr = new();
        TblCode = new();
        Cons = new();
    }
}

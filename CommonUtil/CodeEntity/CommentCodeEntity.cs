﻿using CommonLib.Base;
using System.Collections.Generic;

namespace CommonLib.CodeEntity;

public class CommentCodeEntity : BaseCodeEntity
{
    public List<string> Comments { get; set; }

    public CommentCodeEntity()
    {
        Comments = new();
    }
}

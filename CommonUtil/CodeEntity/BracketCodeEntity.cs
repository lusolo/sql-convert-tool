﻿using CommonLib.Base;

namespace CommonLib.CodeEntity;

public class BracketCodeEntity : BaseCodeEntity
{

    public BaseCodeEntity Code { get; set; }

    public BracketCodeEntity(BaseCodeEntity code)
    {
        Code = code;
    }
}

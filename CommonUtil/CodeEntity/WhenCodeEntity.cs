﻿using CommonLib.Attr;
using CommonLib.Base;

namespace CommonLib.CodeEntity;

public class WhenCodeEntity : BaseCodeEntity
{

    [Order(0)]
    public BaseCodeEntity? Head { get; set; }

}

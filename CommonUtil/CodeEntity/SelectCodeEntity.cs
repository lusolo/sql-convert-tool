﻿using CommonLib.Attr;
using CommonLib.Base;
using System.Collections.Generic;

namespace CommonLib.CodeEntity;

public class SelectCodeEntity : BaseCodeEntity
{
    [Order(0)]
    public List<SqlBlockCodeEntity> Colums { get; set; }

    public int Into { get; set; } = 0;

    [Order(1)]
    public List<BaseCodeEntity> Tables { get; set; }

    [Order(2)]
    public List<BaseCodeEntity> Conditions { get; set; }

    public string? End { get; set; }

    public string? Ustr { get; set; }

    [Order(3)]
    public BaseCodeEntity? Union { get; set; }

    public SelectCodeEntity()
    {
        Colums = new();
        Tables = new();
        Conditions = new();
    }

}

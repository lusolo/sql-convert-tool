﻿using CommonLib.Attr;
using CommonLib.Base;
using System.Collections.Generic;

namespace CommonLib.CodeEntity;

public class ConditionCodeEntity : BaseCodeEntity
{
    [Order(0)]
    public List<BaseCodeEntity> Cons { get; set; }

    public List<string> Marks { get; set; }

    public bool NotFlag { get; set; }

    public ConditionCodeEntity()
    {
        Cons = new();
        Marks = new();
    }
}

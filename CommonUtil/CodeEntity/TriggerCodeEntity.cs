﻿using CommonLib.Base;
using System.Collections.Generic;

namespace CommonLib.CodeEntity;

public class TriggerCodeEntity : BaseCodeEntity
{
    public string Name { get; set; }

    public string Timing { get; set; }

    public string Table { get; set; }

    public bool IsRow { get; set; }

    public List<BaseCodeEntity>? Declare;

    public BaseCodeEntity? Body;

    public TriggerCodeEntity(string name, string timing, string table)
    {
        Name = name;
        Timing = timing;
        Table = table;
    }
}

﻿using CommonLib.Base;
using System.Collections.Generic;

namespace CommonLib.CodeEntity;

public class CaseCodeEntity : BaseCodeEntity
{
    public BaseCodeEntity? Head { get; set; }

    public List<BaseCodeEntity> WhenList { get; set; }

    public BaseCodeEntity? ElseBody { get; set; }

    public CaseCodeEntity() => WhenList = new();

}

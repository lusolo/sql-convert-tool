﻿using CommonLib.Attr;
using CommonLib.Base;
using System.Collections.Generic;

namespace CommonLib.CodeEntity;

public class RootCodeEntity : BaseCodeEntity
{
    [Order(0)]
    public List<BaseCodeEntity> Body { get; set; }

    public RootCodeEntity()
    {
        Body = new();
    }
}

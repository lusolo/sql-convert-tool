﻿using CommonLib.Base;

namespace CommonLib.CodeEntity;

public class SqlBlockCodeEntity : BaseCodeEntity
{
    public string Alias { get; set; } = string.Empty;

    public BaseCodeEntity Bock { get; set; }

    public SqlBlockCodeEntity(string alias, BaseCodeEntity bock)
    {
        this.Alias = alias;
        this.Bock = bock;
    }
}

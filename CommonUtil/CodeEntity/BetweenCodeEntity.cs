﻿using CommonLib.Base;

namespace CommonLib.CodeEntity;

public class BetweenCodeEntity : BaseCodeEntity
{

    public BaseCodeEntity? Head { get; set; }

    public BaseCodeEntity? PreCode { get; set; }

    public BaseCodeEntity? SufCode { get; set; }
}

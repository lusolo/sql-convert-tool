﻿using CommonLib.Base;

namespace CommonLib.CodeEntity;

public class AssignCodeEntity : BaseCodeEntity
{
    public string? Name { get; set; }

    public string? Type { get; set; }

    public string InOut { get; set; }

    public BaseCodeEntity? Defult { get; set; }

    public AssignCodeEntity()
    {
        InOut = string.Empty;
    }
}

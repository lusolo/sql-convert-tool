﻿using CommonLib.Attr;
using CommonLib.Base;
using System.Collections.Generic;

namespace CommonLib.CodeEntity;

public class AndCodeEntity : BaseCodeEntity
{
    public string Mark { get; set; }

    [Order(0)]
    public List<BaseCodeEntity> Condition { get; set; }

    public List<string> Comments { get; set; }

    public bool HasBracket { get; set; }

    public AndCodeEntity()
    {
        Condition = new();
        Comments = new();
        Mark = "AND";
    }
}

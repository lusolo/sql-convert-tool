﻿using CommonLib.Base;

namespace CommonLib.CodeEntity;

public class VerbCodeEntity : BaseCodeEntity
{
    public string Value { get; set; }

    public string Type { get; set; }

    public VerbCodeEntity(string value, string type)
    {
        Value = value;
        Type = type;
    }
}

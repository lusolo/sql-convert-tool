﻿using CommonLib.Base;
using System.Collections.Generic;

namespace CommonLib.CodeEntity;

public class TypeCodeEntity : BaseCodeEntity
{
    public string? Name { get; set; }

    public string Type { get; set; }

    public List<BaseCodeEntity> Objects { get; set; }

    public string? OfType { get; set; }

    public string? IndexBy { get; set; }

    public TypeCodeEntity()
    {
        Objects = [];
        Type = string.Empty;
    }


}

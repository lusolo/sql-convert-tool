﻿using CommonLib.Attr;
using CommonLib.Base;
using System.Collections.Generic;

namespace CommonLib.CodeEntity;

public class FunctionCodeEntity : BaseCodeEntity
{
    public string? Name { get; set; }

    public List<string>? Comments { get; set; }

    [Order(0)]
    public List<BaseCodeEntity> Params { get; set; }

    public string? Return { get; set; }

    [Order(1)]
    public List<BaseCodeEntity>? Declare { get; set; }

    [Order(2)]
    public List<BaseCodeEntity> Body { get; set; }

    public FunctionCodeEntity()
    {
        Params = [];
        Body = [];
    }
}

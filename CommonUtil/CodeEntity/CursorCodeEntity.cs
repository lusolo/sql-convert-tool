﻿using CommonLib.Base;

namespace CommonLib.CodeEntity;

public class CursorCodeEntity : BaseCodeEntity
{
    public string? Name { get; set; }

    public string Type { get; set; } = "Cursor";

    public BaseCodeEntity? Body { get; set; }

}

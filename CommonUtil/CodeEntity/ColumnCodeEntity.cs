﻿using CommonLib.Attr;
using CommonLib.Base;

namespace CommonLib.CodeEntity;

public class ColumnCodeEntity : BaseCodeEntity
{
    public string Name { get; set; }

    public string Kind { get; set; }

    public bool IsNotNull { get; set; }

    public bool PrimayKey { get; set; }

    [Order(0)]
    public BaseCodeEntity? DeflutValue { get; set; }

    public ColumnCodeEntity(string name, string kind, bool isNull)
    {
        Name = name;
        Kind = kind;
        IsNotNull = isNull;
    }
}

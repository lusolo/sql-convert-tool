﻿using CommonLib.Base;
using CommonLib.CodeEntity;
using System.Collections.Generic;

namespace CommonLib.Convert;

public class DeleteConvert(DeleteCodeEntity delete) : BaseConvert
{
    private readonly DeleteCodeEntity Delete = delete;

    protected override List<string> Run()
    {
        var txt = Delete.Head;
        if (Delete.Where!=null)
        {
            txt = $"{txt} {Delete.Where.SqlLine()}";
        }
        return SingleLine(txt!);
    }
}

﻿using CommonLib.Base;
using CommonLib.CodeEntity;
using System.Collections.Generic;

namespace CommonLib.Convert;

public class ReturnConvert(ReturnCodeEntity recode) : BaseConvert
{
    private readonly ReturnCodeEntity ReCode = recode;

    protected override List<string> Run()
    {
        var code = ReCode.Code == null ? string.Empty : ReCode.Code.SqlLine();
        return SingleLine($"RETURN {code}");
    }
}

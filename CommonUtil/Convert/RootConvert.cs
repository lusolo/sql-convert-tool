﻿using CommonLib.Base;
using CommonLib.CodeEntity;
using System.Collections.Generic;

namespace CommonLib.Convert;

public class RootConvert(RootCodeEntity root) : BaseConvert
{
    private readonly RootCodeEntity Root = root;

    protected override List<string> Run()
    {
        var list = new List<string>();
        Root.Body.ForEach(b =>
        {
            list.AddRange(b.SqlList());   
        });
        return list;
    }
}

﻿using CommonLib.Base;
using CommonLib.CodeEntity;
using System.Collections.Generic;

namespace CommonLib.Convert;

public class CommentConvert(CommentCodeEntity comment) : BaseConvert
{
    private readonly CommentCodeEntity Comment = comment;

    protected override List<string> Run()
    {
        return Comment.Comments;
    }
}

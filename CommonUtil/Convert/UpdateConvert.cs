﻿using CommonLib.Base;
using CommonLib.CodeEntity;
using System.Collections.Generic;

namespace CommonLib.Convert;

public class UpdateConvert(UpdateCodeEntity update) : BaseConvert
{

    private readonly UpdateCodeEntity Update = update;

    protected override List<string> Run()
    {
        var list = new List<string>()
        {
            Update.Head!
        };
        for (int i = 0; i < Update.Cols.Count; i++)
        {
            var left = Update.Cols[i];
            var right = Update.Vaules[i].SqlLine();
            list.Add($"{left} = {right},");
        }
        list[^1] = list[^1][..^1];
        if (Update.Where != null)
        {
            list.AddRange(Update.Where.SqlList());
        }
        return list;
    }
}

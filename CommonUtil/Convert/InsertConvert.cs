﻿using CommonLib.Base;
using CommonLib.CodeEntity;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CommonLib.Convert;

public class InsertConvert(InsertCodeEntity insert) : BaseConvert
{
    private readonly InsertCodeEntity Insert = insert;

    protected override List<string> Run()
    {
        var txt = Insert.Head;
        if (Insert.Values.Count == 1)
        {   if(Insert.Values[0] is SelectCodeEntity)
             txt = $"{txt} {Insert.Values[0].SqlLine()}";
            else txt = $"{txt} VAULES {Insert.Values[0].SqlLine()}";
        }
        else txt = $"{txt} VAULES({string.Join(",", Insert.Values.Select(s => s.SqlLine()))})";
        return SingleLine(txt);
    }
}

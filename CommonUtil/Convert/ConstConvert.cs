﻿using CommonLib.Base;
using CommonLib.CodeEntity;
using CommonLib.Info;
using System.Collections.Generic;

namespace CommonLib.Convert;

public class ConstConvert(ConstCodeEntity code) : BaseConvert
{
    private readonly ConstCodeEntity Cont = code;

    protected override List<string> Run()
    {
        var val = Cont.Vaule;
        if (val.StartsWith("STRLIST_"))
        {
            val =SqlStringList.GetItem(val);
        }
        return SingleLine(val);
    }
}

﻿using CommonLib.Base;
using CommonLib.CodeEntity;

namespace SQLServer.Convert;

public class SetConvert(SetCodeEntity set) : BaseConvert
{
    private readonly SetCodeEntity Set = set;

    protected override List<string> Run()
    {
        var left = Set.Left!.SqlLine();
        var right = Set.Right!.SqlLine();
        return SingleLine($"{left} := {right}");
    }
}

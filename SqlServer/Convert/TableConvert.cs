﻿using CommonLib.Base;
using CommonLib.CodeEntity;

namespace SQLServer.Convert;

public class TableConvert(TableCodeEntity tbl) : BaseConvert
{
    private readonly TableCodeEntity Tbl = tbl;

    protected override List<string> Run()
    {
        var list = new List<string>() { $"CREATE TABLE {Tbl.Name}(" };
        list.AddRange(Tbl.Columns.Select(s => string.Format("{0},", s.SqlLine())));
        list.Add($"PRIMARY KEY({Tbl.PrimayKey})");
        list.Add(");");
        return list;
    }
}

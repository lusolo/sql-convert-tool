﻿using CommonLib.Base;
using CommonLib.CodeEntity;

namespace SQLServer.Convert;

public class CaseConvert(CaseCodeEntity Case) : BaseConvert
{

    private readonly CaseCodeEntity Case = Case;

    protected override List<string> Run()
    {
        var list = new List<string>();
        var head = Case.Head == null ? "" : Case.Head.SqlLine();
        Case.WhenList.ForEach(x =>
        {
            list.AddRange(x.SqlList());
        });
        if (Case.ElseBody != null)
        {
            list.AddRange(Case.ElseBody.SqlList());
        }
        return list;
    }
}

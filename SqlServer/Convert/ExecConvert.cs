﻿using CommonLib.Base;
using CommonLib.CodeEntity;

namespace SQLServer.Convert;

public class ExecConvert(ExecCodeEntity exec) : BaseConvert
{
    private readonly ExecCodeEntity Exec = exec;

    protected override List<string> Run()
    {
        var list = new List<string>();
        Exec.Params.ForEach(p =>
        {
            list.Add(p.SqlLine());
        });
        return SingleLine($"{Exec.Name}({string.Join(",",list)})");
    }
}

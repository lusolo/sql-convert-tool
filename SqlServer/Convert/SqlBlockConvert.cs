﻿using CommonLib.Base;
using CommonLib.CodeEntity;

namespace SQLServer.Convert;

public class SqlBlockConvert(SqlBlockCodeEntity sqlBlock) : BaseConvert
{
    private readonly SqlBlockCodeEntity SqlBlock = sqlBlock;

    protected override List<string> Run()
    {
        return SingleLine($"{SqlBlock.Bock.SqlLine()} {SqlBlock.Alias}");
    }
}

﻿using CommonLib.Base;
using CommonLib.CodeEntity;

namespace SQLServer.Convert;

public class JoinConvert(JoinCodeEntity join) : BaseConvert
{
    private readonly JoinCodeEntity Join = join;

    protected override List<string> Run()
    {
        var txt = Join.TblCode[0].SqlLine();
        for (int i = 1; i < Join.TblCode.Count; i++)
        {
            var item = Join.TblCode[i].SqlLine();
            var jstr = Join.Jstr[i - 1];
            txt = $"{txt} {jstr} {item}";
            var cons = Join.Cons[i];
            if (cons != null)
            {
                txt = $"{txt}\r\n{string.Join("\r\n", cons.SqlList())}";
            }
        }
        return SingleLine(txt);
    }
}

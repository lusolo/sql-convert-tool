﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;

namespace SqlConvertTool.DBContext;

public sealed class PostgreSQLDBContext : BaseDBContext
{
    private NpgsqlConnection? Connection;

    protected override DbDataAdapter GetTableAdapter()
    {
        return new NpgsqlDataAdapter("SELECT table_name FROM information_schema.tables", Connection!);
    }

    protected override DbDataAdapter GetViewsAdapter()
    {
        return new NpgsqlDataAdapter("SELECT table_name FROM information_schema.views;", Connection!);
    }

    protected override DbDataAdapter TableDataAdapter(string sql)
    {
        return new NpgsqlDataAdapter(sql, Connection!);
    }

    protected override bool OutPutStore()
    {
        throw new System.NotImplementedException();
    }

    protected override DbConnection ToConnection(string host, string server, string username, string password)
    {
        var conn = string.Format("Server={0};Port=5432; User Id={2};Password={3};Database={1}", host, server, username, password);
        return Connection = new NpgsqlConnection(conn);
    }

    protected override DbCommand TableSqlCommand(string tbl)
    {
        throw new System.NotImplementedException();
    }

    protected override DbCommand ViewSqlCommand(string tbl)
    {
        throw new System.NotImplementedException();
    }

    protected override DataTable GetData(string tbl)
    {
        throw new System.NotImplementedException();
    }

    protected override DbCommandBuilder GetCommandBulider()
    {
        return new NpgsqlCommandBuilder();
    }

    protected override DbTransaction GetTransaction()
    {
        return Connection!.BeginTransaction();
    }

    protected override DbDataAdapter GetTriggerAdapter()
    {
        throw new System.NotImplementedException();
    }

    protected override DbCommand TriggerCommand(string trigger)
    {
        throw new System.NotImplementedException();
    }

    protected override DbDataAdapter GetTypeAdapter()
    {
        throw new System.NotImplementedException();
    }

    protected override DbCommand TypeCommand(string type)
    {
        throw new System.NotImplementedException();
    }

    protected override List<Tuple<string, string>> StoreList(string user)
    {
        throw new System.NotImplementedException();
    }
}

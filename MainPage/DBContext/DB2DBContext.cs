﻿using IBM.Data.Db2;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;

namespace SqlConvertTool.DBContext;

public class DB2DBContext : BaseDBContext
{
    private DB2Connection? Connection;

    protected override DbDataAdapter GetTableAdapter()
    {
        return new DB2DataAdapter("", Connection);
    }

    protected override DbDataAdapter GetViewsAdapter()
    {
        return new DB2DataAdapter("", Connection);
    }

    protected override DbDataAdapter TableDataAdapter(string sql)
    {
        return new DB2DataAdapter(sql, Connection);
    }

    protected override bool OutPutStore()
    {
        throw new NotImplementedException();
    }

    protected override DbConnection ToConnection(string host, string server, string username, string password)
    {
        var conn = string.Format("Server={0};DataBase={1};UID={2};PWD={3}", host, server, username, password);
        return Connection = new(conn);
    }

    protected override DbCommand TableSqlCommand(string tbl)
    {
        throw new NotImplementedException();
    }

    protected override DbCommand ViewSqlCommand(string tbl)
    {
        throw new NotImplementedException();
    }

    protected override DataTable GetData(string tbl)
    {
        throw new NotImplementedException();
    }

    protected override DbCommandBuilder GetCommandBulider()
    {
        return new DB2CommandBuilder();
    }

    protected override DbTransaction GetTransaction()
    {
        return Connection!.BeginTransaction();
    }

    protected override DbDataAdapter GetTriggerAdapter()
    {
        throw new NotImplementedException();
    }

    protected override DbCommand TriggerCommand(string trigger)
    {
        throw new NotImplementedException();
    }

    protected override DbDataAdapter GetTypeAdapter()
    {
        throw new NotImplementedException();
    }

    protected override DbCommand TypeCommand(string type)
    {
        throw new NotImplementedException();
    }

    protected override List<Tuple<string, string>> StoreList(string user)
    {
        throw new NotImplementedException();
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;

namespace SqlConvertTool.DBContext;

public sealed class SQLServerDBContext : BaseDBContext
{
    public SqlConnection? Connection { get; set; }

    protected override DbDataAdapter GetTableAdapter()
    {
        return new SqlDataAdapter("SELECT NAME FROM SYS.tables", Connection);
    }

    protected override DbDataAdapter GetViewsAdapter()
    {
        return new SqlDataAdapter("SELECT NAME FROM SYS.views", Connection);
    }

    protected override DbDataAdapter TableDataAdapter(string sql)
    {
        return new SqlDataAdapter(sql, Connection);
    }

    protected override bool OutPutStore()
    {
        throw new System.NotImplementedException();
    }

    protected override DbConnection ToConnection(string host, string server, string username, string password)
    {
        var conn = string.Format("Server={0};User ID={2};Password={3};Initial Catalog={1};Persist Security Info=True;"
            , host, server, username, password);
        return Connection = new SqlConnection(conn);
    }

    protected override DbCommand TableSqlCommand(string tbl)
    {
        throw new System.NotImplementedException();
    }

    protected override DbCommand ViewSqlCommand(string view)
    {
        var sql = $"SELECT definition FROM sys.sql_modules WHERE object_id=OBJECT_ID('{view}','V')";
        return new SqlCommand(sql, Connection);
    }

    protected override DataTable GetData(string tbl)
    {
        throw new System.NotImplementedException();
    }

    protected override DbCommandBuilder GetCommandBulider()
    {
        return new SqlCommandBuilder();
    }

    protected override DbTransaction GetTransaction()
    {
        return Connection!.BeginTransaction();
    }

    protected override DbDataAdapter GetTriggerAdapter()
    {
        throw new System.NotImplementedException();
    }

    protected override DbCommand TriggerCommand(string trigger)
    {
        throw new System.NotImplementedException();
    }

    protected override DbDataAdapter GetTypeAdapter()
    {
        throw new System.NotImplementedException();
    }

    protected override DbCommand TypeCommand(string type)
    {
        throw new System.NotImplementedException();
    }

    protected override List<Tuple<string, string>> StoreList(string user)
    {
        throw new System.NotImplementedException();
    }
}

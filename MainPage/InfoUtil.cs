﻿using HandyControl.Controls;
using HandyControl.Data;

namespace SqlConvertTool;

public class InfoUtil
{
    public static void Info(string msg, bool IsShowDate = false, int WaitTime = 4)
    {
        var growInfo = new GrowlInfo
        {
            Message = msg,
            ShowDateTime = IsShowDate,
            WaitTime = WaitTime
        };
        Growl.Info(growInfo);
    }

    public static void Warng(string msg, bool IsShowDate = false, int WaitTime = 4)
    {
        var growInfo = new GrowlInfo
        {
            Message = msg,
            ShowDateTime = IsShowDate,
            WaitTime = WaitTime
        };
        Growl.Warning(growInfo);
    }

    public static void Error(string msg, bool IsShowDate = false, int WaitTime = 4)
    {
        var growInfo = new GrowlInfo
        {
            Message = msg,
            ShowDateTime = IsShowDate,
            WaitTime = WaitTime
        };
        Growl.Error(growInfo);
    }

    public static void Success(string msg, bool IsShowDate = false, int WaitTime = 4)
    {
        var growInfo = new GrowlInfo
        {
            Message = msg,
            ShowDateTime = IsShowDate,
            WaitTime = WaitTime
        };
        Growl.Success(growInfo);
    }
}

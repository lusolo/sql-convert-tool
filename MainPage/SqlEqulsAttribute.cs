﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SqlConvertTool;

public class SqlEqulsAttribute : ValidationAttribute
{
    protected override ValidationResult IsValid(object? value, ValidationContext validationContext)
    {       
        var arr = (value as string)!.Split("TO",StringSplitOptions.TrimEntries);
        if (arr.Length == 2 && !arr[1].Equals(arr[0])) return ValidationResult.Success!;
        return new("Before Language is same as After Language");
    }
}
